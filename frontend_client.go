// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"context"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/redfield/split/internal/api"
	"gitlab.com/redfield/split/metadata"
)

// FrontendClient provides a client for the Frontend service.
type FrontendClient struct {
	c api.FrontendClient

	// Keep track so it can be closed later.
	cc *grpc.ClientConn

	// Timeout for RPC calls.
	timeout time.Duration
}

// NewFrontendClient returns a new Frontend client.
func NewFrontendClient(cc *grpc.ClientConn) *FrontendClient {
	fc := &FrontendClient{
		c:       api.NewFrontendClient(cc),
		cc:      cc,
		timeout: 30 * time.Second,
	}

	return fc
}

// SetTimeout sets the timeout used to call non-streaming RPCs.
func (fc *FrontendClient) SetTimeout(timeout time.Duration) {
	fc.timeout = timeout
}

// Close closes the frontend client connection.
func (fc *FrontendClient) Close() error {
	return fc.cc.Close()
}

// notify sends a notification to a frontend.
//
// For now, this should only be needed internally.
func (fc *FrontendClient) notify(n *api.Notification) error {
	r := &api.NotifyRequest{
		Notification: n,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.Notify(ctx, r)

	return err
}

// GetMetaData returns the FrontendMetaData.
func (fc *FrontendClient) GetMetaData() (*metadata.FrontendMetaData, error) {
	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	resp, err := fc.c.GetMetaData(ctx, &api.GetMetaDataRequest{})
	if err != nil {
		return nil, err
	}

	return resp.Metadata, nil
}
