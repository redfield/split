// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"fmt"

	"gitlab.com/redfield/split/metadata"
)

const (
	transportTCP  = "tcp"
	transportUnix = "unix"
	transportArgo = "argo"
)

type protoUnsupportedErr string

func (p protoUnsupportedErr) Error() string {
	return "protocol '" + string(p) + "' is not supported"
}

// Transport represents a transport specifier for a frontend or backend.
type Transport struct {
	Info *metadata.TransportInfo
}

// NewIPv4Transport returns a Transport for IPv4.
func NewIPv4Transport(addr, port string) *Transport {
	ti := &metadata.TransportInfo{
		Type: metadata.TransportInfo_IPV4,
		Destination: &metadata.TransportInfo_Ipv4{
			Ipv4: &metadata.DestinationIPv4{
				Address: addr,
				Port:    port,
			},
		},
	}

	return &Transport{ti}
}

// NewIPv6Transport returns a Transport for IPv6.
func NewIPv6Transport(addr, port string) *Transport {
	ti := &metadata.TransportInfo{
		Type: metadata.TransportInfo_IPV6,
		Destination: &metadata.TransportInfo_Ipv6{
			Ipv6: &metadata.DestinationIPv6{
				Address: addr,
				Port:    port,
			},
		},
	}

	return &Transport{ti}
}

// NewSocketTransport returns a Transport for Unix domain sockets.
func NewSocketTransport(path string) *Transport {
	ti := &metadata.TransportInfo{
		Type: metadata.TransportInfo_SOCKET,
		Destination: &metadata.TransportInfo_Socket{
			Socket: &metadata.DestinationSocket{
				Path: path,
			},
		},
	}

	return &Transport{ti}
}

// NewArgoTransport returns a Transport for Argo based connections.
func NewArgoTransport(domid, port string) *Transport {
	ti := &metadata.TransportInfo{
		Type: metadata.TransportInfo_ARGO,
		Destination: &metadata.TransportInfo_Argo{
			Argo: &metadata.DestinationArgo{
				Domid: domid,
				Port:  port,
			},
		},
	}

	return &Transport{ti}
}

// Network returns the network name.
func (t Transport) Network() string {
	switch t.Info.GetType() {
	case metadata.TransportInfo_IPV4, metadata.TransportInfo_IPV6:
		return transportTCP
	case metadata.TransportInfo_SOCKET:
		return transportUnix
	case metadata.TransportInfo_ARGO:
		return transportArgo
	default:
		return ""
	}
}

// String returns the string form of the destination address.
func (t Transport) String() string {
	switch t.Info.GetType() {
	case metadata.TransportInfo_IPV4:
		d := t.Info.GetIpv4()

		if d.Address != "" && d.Port != "" {
			return fmt.Sprintf("%v:%v", d.Address, d.Port)
		}

		if d.Address != "" {
			return d.Address
		}

		if d.Port != "" {
			return fmt.Sprintf(":%v", d.Port)
		}

		return ""

	case metadata.TransportInfo_IPV6:
		d := t.Info.GetIpv6()

		if d.Address != "" && d.Port != "" {
			return fmt.Sprintf("[%v]:%v", d.Address, d.Port)
		}

		if d.Address != "" {
			return d.Address
		}

		if d.Port != "" {
			return fmt.Sprintf("[::]:%v", d.Port)
		}

		return ""

	case metadata.TransportInfo_SOCKET:
		d := t.Info.GetSocket()

		return d.Path

	case metadata.TransportInfo_ARGO:
		d := t.Info.GetArgo()

		return fmt.Sprintf("%s:%s", d.Domid, d.Port)

	default:
		return ""
	}
}
