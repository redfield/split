// Copyright 2021 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"net"
	"net/url"
	"strings"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

type winProtoUnsupportedErr string

func (p winProtoUnsupportedErr) Error() string {
	return "protocol '" + string(p) + "' is not supported on windows"
}

// ParseTransport returns a new transport by parsing the given
// dest string. An error is returned if the scheme is not supported
// or the dest cannot be parsed.
func ParseTransport(dest string) (*Transport, error) {
	u, err := url.Parse(dest)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse dest string")
	}

	switch u.Scheme {
	case transportTCP:
		// According to https://golang.org/pkg/net/url/#URL.Hostname,
		// Hostname always strips the port number if present. So, we
		// can use the presence of ':' characters in the hostname to
		// indicate an IPv6 address.
		if strings.Contains(u.Hostname(), ":") {
			return NewIPv6Transport(u.Hostname(), u.Port()), nil
		}

		return NewIPv4Transport(u.Hostname(), u.Port()), nil

	case transportUnix, transportArgo:
		return nil, winProtoUnsupportedErr(u.Scheme)

	default:
		return nil, protoUnsupportedErr(u.Scheme)
	}
}

// ClientConn creates a new grpc.ClientConn from the Transport.
func (t *Transport) ClientConn() (*grpc.ClientConn, error) {
	var target string

	opts := []grpc.DialOption{grpc.WithInsecure()}

	switch t.Network() {
	case transportTCP:
		target = t.String()

	case transportUnix, transportArgo:
		return nil, winProtoUnsupportedErr(t.Network())

	default:
		return nil, protoUnsupportedErr(t.Network())
	}

	return grpc.Dial(target, opts...)
}

// Listener returns a new net.Listener built from the Transport.
func (t *Transport) Listener() (net.Listener, error) {
	switch t.Network() {
	case transportTCP:
		return net.Listen(t.Network(), t.String())

	case transportUnix, transportArgo:
		return nil, winProtoUnsupportedErr(t.Network())

	default:
		return nil, protoUnsupportedErr(t.Network())
	}
}
