module gitlab.com/redfield/split

go 1.14

require (
	github.com/golang/protobuf v1.4.3
	github.com/pkg/errors v0.9.1
	gitlab.com/redfield/go-argo v0.0.0-20210119182033-bf1730a6cdb9
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	google.golang.org/grpc v1.34.1
	google.golang.org/protobuf v1.25.0
)
