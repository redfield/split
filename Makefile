prefix ?= /usr/local
includedir ?= $(prefix)/include

metadata/metadata.pb.go:
	protoc -I . \
	    --go_out=metadata \
	    --go_opt=module=gitlab.com/redfield/split/metadata \
	    metadata/metadata.proto

internal/api/split.pb.go: internal/api/split.proto metadata/metadata.pb.go
	protoc -I . \
	    --go_out=internal/api \
	    --go_opt=module=gitlab.com/redfield/split/internal/api \
	    internal/api/split.proto

internal/api/split_grpc.pb.go: internal/api/split.proto metadata/metadata.pb.go
	protoc -I . \
	    --go-grpc_out=internal/api \
	    --go-grpc_opt=module=gitlab.com/redfield/split/internal/api \
	    internal/api/split.proto

.PHONY: proto
proto: internal/api/split.pb.go internal/api/split_grpc.pb.go

.PHONY: test
test:
	go test -v ./

.PHONY: golint
golint:
	golangci-lint --verbose run

.PHONY: install-proto
install-proto:
	install -d -m 0755 $(includedir)/split
	install -d -m 0755 $(includedir)/split/metadata
	install -m 0644 internal/api/split.proto $(includedir)/split/split.proto
	install -m 0644 metadata/metadata.proto $(includedir)/split/metadata/metadata.proto
