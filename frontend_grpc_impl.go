// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"context"
	"sync"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split/internal/api"
	"gitlab.com/redfield/split/metadata"
)

// frontendServer is the type used to actually implement the
// api.FrontendServer interface. This allows a clean separation
// between the public API available via the backend type, and
// the gRPC API implementation, which is not intended to be called
// directly.
type frontendServer struct {
	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedFrontendServer

	Metadata *metadata.FrontendMetaData

	// Keep track of backend meta data used
	// for backend client
	bmd *metadata.BackendMetaData
	bc  *BackendClient

	init *initializer

	// ctx and cancel func for frontend are used for goroutines
	// that are created as part of initialization, and need to be
	// killed during teardown.
	ctx    context.Context
	cancel context.CancelFunc

	// gRPC server for the frontend
	server *grpc.Server
}

func (f *frontendServer) GetMetaData(ctx context.Context, in *api.GetMetaDataRequest) (*api.GetMetaDataReply, error) {
	return &api.GetMetaDataReply{Metadata: f.Metadata}, nil
}

// Notify is used to notify a frontend of certain events such as initialization ACK's, and a backend going down.
func (f *frontendServer) Notify(ctx context.Context, r *api.NotifyRequest) (*api.NotifyReply, error) {
	if err := f.handleNotify(r.GetNotification()); err != nil {
		return nil, err
	}

	return &api.NotifyReply{}, nil
}

// initState represents the initialization state.
type initState int32

const (
	initStateActive initState = iota + 1
	initStateFailed
	initStateCompleted
	initStateBackendLost
)

// initializer is a helper for the initialization phase for netctl
type initializer struct {
	mu    sync.Mutex
	state initState

	local  chan error
	remote chan error
}

func (i *initializer) setState(state initState) {
	i.mu.Lock()
	defer i.mu.Unlock()

	i.state = state
}

func (i *initializer) getState() initState {
	i.mu.Lock()
	defer i.mu.Unlock()

	return i.state
}

func (f *frontendServer) initialize(nb *metadata.BackendMetaData, timeout time.Duration) error {
	// If the init state is active or completed, bail out.
	//
	// Note that this means initialization can begin if the state is undefined,
	// failed, or backend lost.
	if s := f.init.getState(); s == initStateCompleted || s == initStateActive {
		return errors.Errorf("cannot begin initialization phase due to current init state (state=%v)", s)
	}

	// Allocate buffered channels for init phase.
	f.init.local = make(chan error, 1)
	f.init.remote = make(chan error, 1)

	// Begin the initialization phase.
	f.init.setState(initStateActive)

	// (1) Create a backend client before starting any other init
	//     handlers.
	bc, err := createBackendClient(nb)
	if err != nil {
		f.init.setState(initStateFailed)

		return errors.Wrap(err, "failed to create backend client")
	}

	// (2) Create an error channel to wait for the init
	//     error handler, and start the handler in another
	//     goroutine.
	ec := make(chan error)
	go func() {
		ec <- f.initializationErrorHandler(timeout)
	}()

	// (3) Try to register with the backend. Send any errors over
	//     init.remote so that the init error handler can catch them.
	go func() {
		f.init.remote <- bc.RegisterFrontend(f.Metadata)
	}()

	// (4) Wait for the verdict from the init error handler.
	//     Note that init.local will be set from handleRegisterAck,
	//     which is triggered by the backend calling Notify.
	if err := <-ec; err != nil {
		f.init.setState(initStateFailed)
		bc.Close()

		return err
	}

	// (5) Set the frontend's backend client and save the
	//     backend metadata.
	f.bc = bc
	f.bmd = nb

	// (6) Finally, set init state to completed.
	f.init.setState(initStateCompleted)

	return nil
}

// initializationErrorHandler watches the frontend's initializer to determine
// if an error occurs remotely or locally, and ensures that the first error
// encountered is the one returned.
func (f *frontendServer) initializationErrorHandler(timeout time.Duration) error {
	var eg errgroup.Group

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	eg.Go(func() error {
		select {
		case err := <-f.init.local:
			return errors.Wrap(err, "init failed locally")
		case <-ctx.Done():
			return ctx.Err()
		}
	})

	eg.Go(func() error {
		select {
		case err := <-f.init.remote:
			return errors.Wrap(err, "init failed remotely")
		case <-ctx.Done():
			return ctx.Err()
		}
	})

	return eg.Wait()
}

// handleNotify is called by the Notify RPC.
func (f *frontendServer) handleNotify(n *api.Notification) error {
	switch n.GetType() {
	case api.Notification_REGISTER_ACK:
		return f.handleRegisterAck(n.GetFrontUuid())

	case api.Notification_BACKEND_LEAVING:
		return f.handleBackendLeaving(n.GetFrontUuid())

	default:
		return errors.Errorf("unable to handle notification of type %v", n.GetType())
	}
}

// handleRegisterAck is used during the initialization phase, and is
// how a backend ACK's the frontend registration.
func (f *frontendServer) handleRegisterAck(uuid string) error {
	if f.init.getState() != initStateActive {
		return errors.New("initialization is not active")
	}

	var err error

	if uuid != f.Metadata.Uuid {
		err = errors.New("this frontend does not match requested frontend")
	}

	f.init.local <- err

	return err
}

// handleBackendLeaving is called when a backend has notified the frontend
// that it is closing.
func (f *frontendServer) handleBackendLeaving(uuid string) error {
	if f.init.getState() != initStateCompleted {
		return errors.New("frontend is not registered with any backend")
	}

	if uuid != f.Metadata.Uuid {
		return errors.New("this frontend does not match requested frontend")
	}

	f.init.setState(initStateBackendLost)

	f.bc.Close()

	go f.waitForLostBackend()

	return nil
}

func createBackendClient(nb *metadata.BackendMetaData) (*BackendClient, error) {
	ti := &Transport{Info: nb.TransportInfo}

	if ti.String() == "" {
		return nil, errors.New("transport unspecified")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	bc := NewBackendClient(cc)

	// Set a reasonable timeout for RPCs.
	bc.SetTimeout(10 * time.Second)

	return bc, nil
}

func (f *frontendServer) unregisterWithBackend() error {
	if s := f.init.getState(); s != initStateCompleted {
		return errors.Errorf("frontend is not registered with any backend (state=%v)", s)
	}

	return f.bc.UnregisterFrontend(f.Metadata)
}

func (f *frontendServer) waitForLostBackend() {
	// It's likely that the backend is just restarting.
	// Start off with an aggressive retry rate, and then
	// ease off after a few failures.
	for i := 0; i < 3; i++ {
		err := f.initialize(f.bmd, 5*time.Second)
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}

		return
	}

	for {
		select {
		case <-f.ctx.Done():
			return
		default:
			err := f.initialize(f.bmd, 5*time.Second)
			if err != nil {
				time.Sleep(10 * time.Second)
				continue
			}

			return
		}
	}
}
