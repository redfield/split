// Copyright 2021 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"context"
	"fmt"
	"net"
	"net/url"
	"os"
	"strings"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/go-argo/argo"
)

// ParseTransport returns a new transport by parsing the given
// dest string. An error is returned if the scheme is not supported
// or the dest cannot be parsed.
func ParseTransport(dest string) (*Transport, error) {
	u, err := url.Parse(dest)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse dest string")
	}

	switch u.Scheme {
	case transportTCP:
		// According to https://golang.org/pkg/net/url/#URL.Hostname,
		// Hostname always strips the port number if present. So, we
		// can use the presence of ':' characters in the hostname to
		// indicate an IPv6 address.
		if strings.Contains(u.Hostname(), ":") {
			return NewIPv6Transport(u.Hostname(), u.Port()), nil
		}

		return NewIPv4Transport(u.Hostname(), u.Port()), nil

	case transportUnix:
		return NewSocketTransport(u.Path), nil

	case transportArgo:
		return NewArgoTransport(u.Hostname(), u.Port()), nil

	default:
		return nil, protoUnsupportedErr(u.Scheme)
	}
}

// ClientConn creates a new grpc.ClientConn from the Transport.
func (t *Transport) ClientConn() (*grpc.ClientConn, error) {
	var target string

	opts := []grpc.DialOption{grpc.WithInsecure()}

	// grpc.Dial doesn't seem to play well with the net.Addr
	// interface. Namely, passing scheme://name works for Unix,
	// but not for TCP.
	//
	// Make sure the target string is well-formatted before calling
	// Dial.
	switch t.Network() {
	case transportUnix:
		target = fmt.Sprintf("%s://%s", t.Network(), t.String())
	case transportTCP:
		target = t.String()
	case transportArgo:
		target = t.String()
		opts = append(opts, grpc.WithContextDialer(
			func(ctx context.Context, addr string) (net.Conn, error) {
				return argo.DialStream(addr)
			},
		))
	}

	return grpc.Dial(target, opts...)
}

// Listener returns a new net.Listener built from the Transport.
func (t *Transport) Listener() (net.Listener, error) {
	switch t.Network() {
	case transportTCP:
		return net.Listen(t.Network(), t.String())
	case transportUnix:
		lis, err := net.Listen(t.Network(), t.String())
		if err != nil {
			return lis, err
		}

		// Set permissions of the socket so that is world-writable.
		if err := os.Chmod(t.String(), 0777); err != nil {
			lis.Close()

			return nil, errors.Wrap(err, "failed to create socket listener")
		}

		return lis, nil
	case transportArgo:
		return argo.ListenStream(t.String())
	default:
		return nil, protoUnsupportedErr(t.Network())
	}
}
