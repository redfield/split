// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"errors"
	"fmt"
	"math/rand"
	"net"
	"testing"
	"time"

	"golang.org/x/sync/errgroup"
)

// newFrontendBackendPair returns a Frontend and a Backend
// that can be used for testing.
func newFrontendBackendPair(t *testing.T) (*Frontend, *Backend) {
	// Randomize the last three digits, and use in each address.

	// #nosec
	r := 100 + rand.Intn(899)
	faddr := fmt.Sprintf("tcp://localhost:9%d", r)
	baddr := fmt.Sprintf("tcp://localhost:8%d", r)

	f, err := NewFrontend(faddr)
	if err != nil {
		t.Fatalf("failed to create frontend: %v", err)
	}
	f.Metadata.Uuid = "test-frontend"

	b, err := NewBackend(baddr)
	if err != nil {
		f.Close()
		t.Fatalf("failed to create backend: %v", err)
	}
	b.Metadata.Uuid = "test-backend"

	return f, b
}

func TestInitializationAndTeardown(t *testing.T) {
	var eg errgroup.Group

	f, b := newFrontendBackendPair(t)

	eg.Go(func() error { return f.Serve() })
	eg.Go(func() error { return b.Serve() })
	eg.Go(func() error {
		err := f.Initialize(b.Metadata, 5*time.Second)
		if err != nil {
			return err
		}

		if err := f.Close(); err != nil {
			return err
		}

		if err := b.Close(); err != nil {
			return err
		}

		return nil
	})

	if err := eg.Wait(); err != nil {
		t.Errorf("Faild to initialize frontend: %v", err)
	}
}

func TestRestartBackend(t *testing.T) {
	var eg errgroup.Group

	f, b := newFrontendBackendPair(t)

	eg.Go(func() error { return f.Serve() })
	eg.Go(func() error { return b.Serve() })
	eg.Go(func() error {
		err := f.Initialize(b.Metadata, 5*time.Second)
		if err != nil {
			return err
		}

		// Restart the backend
		err = b.Close()
		if err != nil {
			f.Close()
			return err
		}

		eg.Go(func() error { return b.Serve() })
		// Confirm that the frontend has re-registered,
		// but give it some time.
		eg.Go(func() error {
			// In either case, the frontend and backend need to
			// be closed at the end of this function (frontend first).
			defer b.Close()
			defer f.Close()

			for {
				// Check every 500 millisec, up to 10 seconds, for the frontend
				// to re-register.
				select {
				case <-time.After(10 * time.Second):
					return errors.New("frontend did not come back after 10 secs")

				case <-time.After(500 * time.Millisecond):
					_, err := b.GetFrontendMetaData(f.Metadata.Uuid)
					if err != nil {
						continue
					}

					// The frontend registered again!
					return nil
				}
			}
		})

		return nil
	})

	if err := eg.Wait(); err != nil {
		t.Errorf("Failed to handle restarting backend: %v", err)
	}
}

func TestRestartFrontend(t *testing.T) {
	var eg errgroup.Group

	f, b := newFrontendBackendPair(t)

	eg.Go(func() error { return f.Serve() })
	eg.Go(func() error { return b.Serve() })
	eg.Go(func() error {
		defer b.Close()

		err := f.Initialize(b.Metadata, 5*time.Second)
		if err != nil {
			return err
		}

		// Restart the frontend...
		if err := f.Close(); err != nil {
			return err
		}

		t := &Transport{f.Metadata.TransportInfo}

		f, err = NewFrontend(fmt.Sprintf("%s://%s", t.Network(), t.String()))
		if err != nil {
			return err
		}
		defer f.Close()

		f.Metadata.Uuid = "test-frontend-2"

		eg.Go(func() error { return f.Serve() })

		err = f.Initialize(b.Metadata, 5*time.Second)
		if err != nil {
			return err
		}

		return nil
	})

	if err := eg.Wait(); err != nil {
		t.Errorf("Faild to initialize frontend: %v", err)
	}
}

func TestInitializationTimeout(t *testing.T) {
	var eg errgroup.Group

	f, b := newFrontendBackendPair(t)

	eg.Go(func() error { return f.Serve() })

	// Don't start the backend server, start a non-responsive
	// TCP server in its place.
	eg.Go(func() error {
		t := &Transport{b.Metadata.TransportInfo}

		ln, err := net.Listen(t.Network(), t.String())
		if err != nil {
			return err
		}
		defer ln.Close()

		_, err = ln.Accept()
		if err != nil {
			return err
		}

		return nil
	})

	eg.Go(func() error {
		defer f.Close()

		err := f.Initialize(b.Metadata, 3*time.Second)
		if err == nil {
			return errors.New("expected initialization timeout")
		}

		return nil
	})

	if err := eg.Wait(); err != nil {
		t.Errorf("Faild to test initialization timeout: %v", err)
	}
}
