// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split/internal/api"
	"gitlab.com/redfield/split/metadata"
)

// Backend implements the Back service.
type Backend struct {
	s        *backendServer
	Metadata *metadata.BackendMetaData

	opts *backendOptions
}

// NewBackend returns a new Backend.
func NewBackend(addr string, opts ...BackendOption) (*Backend, error) {
	ti, err := ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "invald address")
	}

	s := &backendServer{
		frontends: make(map[string]*metadata.FrontendMetaData),
	}

	b := &Backend{
		s: s,
		Metadata: &metadata.BackendMetaData{
			TransportInfo: ti.Info,
		},
		opts: &backendOptions{},
	}

	for _, opt := range opts {
		opt.apply(b.opts)
	}

	return b, nil
}

// BackendOption is used to specify options for a Backend.
type BackendOption interface {
	apply(*backendOptions)
}

type backendOptions struct {
	secondListenerAddr string
}

type funcBackendOption struct {
	f func(*backendOptions)
}

func (fbo *funcBackendOption) apply(s *backendOptions) {
	fbo.f(s)
}

func newFuncBackendOption(f func(*backendOptions)) *funcBackendOption {
	return &funcBackendOption{f}
}

// WithSecondListener specifies a second address to have the registered
// services listen on. This is especially useful when a local, consistent
// address is needed for clients to communicate with the backend, as opposed
// to the address used for frontend services to connect to.
func WithSecondListener(addr string) BackendOption {
	return newFuncBackendOption(func(bo *backendOptions) {
		bo.secondListenerAddr = addr
	})
}

// Serve starts the Backend server. It accepts variadic list of func's
// that accept a grpc.Server. This allows callers to supply a function
// to call in order to register their service on this Server.
//
// E.g., a caller might do:
//
//     f.Serve(func(s *grpc.Server) {
//             RegisterNetctlBackServer(s, myNetctlServer)
//     })
func (b *Backend) Serve(rfuncs ...func(s *grpc.Server)) error {
	var eg errgroup.Group

	eg.Go(func() error {
		return b.servePrimary(rfuncs...)
	})

	if b.opts.secondListenerAddr != "" {
		eg.Go(func() error {
			return b.serveSecondary(rfuncs...)
		})
	}

	return eg.Wait()
}

func (b *Backend) servePrimary(rfuncs ...func(s *grpc.Server)) error {
	t := &Transport{b.Metadata.TransportInfo}

	lis, err := t.Listener()
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	b.s.server = grpc.NewServer()
	api.RegisterBackendServer(b.s.server, b.s)

	for _, register := range rfuncs {
		register(b.s.server)
	}

	return b.s.server.Serve(lis)
}

func (b *Backend) serveSecondary(rfuncs ...func(s *grpc.Server)) error {
	t, err := ParseTransport(b.opts.secondListenerAddr)
	if err != nil {
		return err
	}

	lis, err := t.Listener()
	if err != nil {
		return errors.Wrap(err, "failed to create secondary listener")
	}
	defer lis.Close()

	b.s.secondServer = grpc.NewServer()
	api.RegisterBackendServer(b.s.secondServer, b.s)

	for _, register := range rfuncs {
		register(b.s.secondServer)
	}

	return b.s.secondServer.Serve(lis)
}

// Close tears down the Backend.
func (b *Backend) Close() error {
	failedNotify := false

	for _, f := range b.s.getAllFrontends() {
		err := b.s.notifyBackendClosing(f)
		if err != nil {
			failedNotify = true
		}
	}

	if b.s.server != nil {
		b.s.server.Stop()
	}

	if b.s.secondServer != nil {
		b.s.secondServer.Stop()
	}

	if failedNotify {
		return errors.New("failed to clean up all frontends")
	}

	return nil
}

// SetRegisterHandler sets the function that should be called as a result
// of a frontend successfully registering with the backend.
func (b *Backend) SetRegisterHandler(handler func(f *metadata.FrontendMetaData)) {
	b.s.mu.Lock()
	defer b.s.mu.Unlock()

	b.s.onRegisterFrontend = handler
}

// SetUnregisterHandler sets the function that should be called as a result
// of a frontend unregistering with the backend.
func (b *Backend) SetUnregisterHandler(handler func(f *metadata.FrontendMetaData)) {
	b.s.mu.Lock()
	defer b.s.mu.Unlock()

	b.s.onUnregisterFrontend = handler
}

// GetFrontendMetaData returns metadata for a frontend with a matching UUID, if it exists.
// An error is return if no matching frontend is found.
func (b *Backend) GetFrontendMetaData(uuid string) (*metadata.FrontendMetaData, error) {
	return b.s.getFrontend(uuid)
}

// GetAllFrontendMetaData returns a list of metadata for all registered frontends.
func (b *Backend) GetAllFrontendMetaData() []*metadata.FrontendMetaData {
	return b.s.getAllFrontends()
}
