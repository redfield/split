// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split/internal/api"
	"gitlab.com/redfield/split/metadata"
)

type Frontend struct {
	s        *frontendServer
	Metadata *metadata.FrontendMetaData
}

func NewFrontend(addr string) (*Frontend, error) {
	ti, err := ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "invalid address")
	}

	ctx, cancel := context.WithCancel(context.Background())

	// Use the same pointer to metadata for the Frontend
	// and the frontendServer.
	md := &metadata.FrontendMetaData{
		TransportInfo: ti.Info,
	}

	frontend := &Frontend{
		Metadata: md,
		s: &frontendServer{
			init:     &initializer{},
			ctx:      ctx,
			cancel:   cancel,
			Metadata: md,
		},
	}

	return frontend, nil
}

// Initialize runs the initailization process against the Backend specified by
// the given metadata.BackendMetaData.
func (f *Frontend) Initialize(nb *metadata.BackendMetaData, timeout time.Duration) error {
	err := f.s.initialize(nb, timeout)
	if err != nil {
		return err
	}

	return nil
}

// Serve starts the Frontend server. It accepts variadic list of func's
// that accept a grpc.Server. This allows callers to supply a function
// to call in order to register their service on this Server.
//
// E.g., a caller might do:
//
//     f.Serve(func(s *grpc.Server) {
//             RegisterNetctlFrontServer(s, myNetctlServer)
//     })
func (f *Frontend) Serve(rfuncs ...func(s *grpc.Server)) error {
	t := &Transport{f.Metadata.TransportInfo}

	lis, err := t.Listener()
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	f.s.server = grpc.NewServer()
	api.RegisterFrontendServer(f.s.server, f.s)

	for _, register := range rfuncs {
		register(f.s.server)
	}

	return f.s.server.Serve(lis)
}

func (f *Frontend) Close() error {
	var e error

	if err := f.s.unregisterWithBackend(); err != nil {
		e = errors.Wrap(err, "failed to unregister with backend")
	}

	if f.s.bc != nil {
		f.s.bc.Close()
	}

	f.s.init.setState(0)

	// Stop all goroutines that are known to the frontend.
	// Then, do a graceful stop on the gRPC server.
	//
	// It is necessary that f.cancel is called before the server is
	// stopped, as active RPCs may not return until a monitor has
	// died.
	f.s.cancel()

	if f.s.server != nil {
		f.s.server.GracefulStop()
	}

	return e
}
